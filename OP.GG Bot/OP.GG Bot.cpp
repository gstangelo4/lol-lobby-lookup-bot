#include "pch.h"
#include <wchar.h>

using namespace System;

#define SUMMONER_DELIMETER L" joined the lobby"

int main()
{
	// grab commandline args
	array<String^>^ args = Environment::GetCommandLineArgs();

	// setup vars
	String^ homepage = "https://na.op.gg/";
	String^ target = "https://na.op.gg/summoner/userName=";
	Collections::Generic::List<String^>^ summoners = gcnew Collections::Generic::List<String^>();
	
	// prompt user
	Console::WriteLine("------------------------------ Welcome to OP.GG Bot! ------------------------------"
		+ Environment::NewLine
		+ Environment::NewLine
		+ "Paste text that gets generated from entering a pregame lobby into the bot."
		+ Environment::NewLine
		+ "Enter a blank line to submit request."
		+ Environment::NewLine
		+ Environment::NewLine
		+ "Application will automatically close once it has finished processing your request."
		+ Environment::NewLine
		+ Environment::NewLine
		+ "::::::::::::::::::::::::::::::::::::DISCLAIMER::::::::::::::::::::::::::::::::::::"
		+ Environment::NewLine
		+ Environment::NewLine
		+ "If the text entered does not match the specific pregame lobby syntax then the"
		+ Environment::NewLine
		+ "request will not work for the affected lines."
		+ Environment::NewLine
		+ "-----------------------------------------------------------------------------------"
		+ Environment::NewLine);

	Console::WriteLine("Enter summoner text:");
	
	// read buffer until CTRL+Z is entered
	for (;;)
	{
		String^ line = Console::ReadLine();

		if (line && !String::IsNullOrWhiteSpace(line))
		{
			summoners->Add(line);
		}
		else
		{
			break;
		}
	}

	// open homepage wait for browser to catch up
	Diagnostics::Process::Start(homepage);
	Threading::Thread::Sleep(250);

	// open valid summoner names
	for (int i = 0; i < summoners->Count; ++i)
	{
		String^ summonerName;

		if (summoners[i]->EndsWith(SUMMONER_DELIMETER))
		{
			summonerName = summoners[i]->Substring(0, summoners[i]->Length - wcslen(SUMMONER_DELIMETER));
		}

		if (!String::IsNullOrEmpty(summonerName))
		{
			// wait for browser to catch up
			Threading::Thread::Sleep(250);
			Diagnostics::Process::Start(target + summonerName);
		}
	}

	return 0;
}

// TESTCASE

/*
UWANA ME DIQ joined the lobby
Nikong joined the lobby
David1996Lee joined the lobby
TSM zocton joined the lobby
Hanhie joined the lobby
*/